## Where the fuck is my distopya??
We live in a time so fucked up that even the romantic representation of unauspicable societies seems auspicable as of right now

aka: I'd much rather live in a cyberpunk turbocapitalist hellscape than here - it would still suck, but at least it would be cool

---

We are way past the point where any sane person would see any benefit in pursuing the dreams of capitalist success, except some questionable "sigma grindset" self help pseudophilosophy shit 

But we are also not at a totally dehumanized point of history where we all recognize our inevitable doom, yet find comfort in loving other ppl with that sort of comradery that forms when you share the same ugly fate

As of right now, we are just a bunch of people rotting on their own, pretending to enjoy life with stupid consumistic rituals, like clubs, alchool, fake "romantic" relationships that completely distort the concept of love in favor of creating an image for the others to see how less bad we are doing.

We continuously live bouncing between envying others for their displayed lives and trying to make others evious of our illusory achievments and well being.

We uphold undefendable social norms and customs, like the afromentioned "romantic" replationship, that do not allign at all to how people actually could feel and enjoy love, in a free, undefined way, where each relationship is uniquely defined by its dynamics and which part of the [lexic of affection](../theories/lexic-affection.md) are allowed and used as vectors of meaning _or_ as mere sources of physical pleasure _or_ as both.