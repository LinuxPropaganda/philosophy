# Lexic of Affection

## Framework
To understand how people interact, feel and love, we need to escape from the sick framework of traditional relationships like "friendship" or "romantic".

People love each other, in as many different ways as many the linear combinations of all the people on earth.

"Love" here is defined weakly, as a synonym of affection. Later we will frame a tighter, stricter definition, making it a describer of _just some_ relationships.

## What is a relationship, really?

It is consensus that the whole meaning of bonding with others is satisfaction, through self growth, support, fun, and other stuff that we generally want from other ppl.

This king of bonding inevitably produces affection. Affection is defined by the general _good leaning_ towards a person, if you feel good with someone and want to stay with them, you probably feel affection towards them.

We can now subvert the consensus previously stated, by saying that once affection is established, we do seek to get **and** provide that satisfaction from/to the other person _because of the affection we feel_. [omg I actually agree on something with that creepy old degenerate?](https://penelope.uchicago.edu/Thayer/E/Roman/Texts/Cicero/Laelius_de_Amicitia/text*.html)

With this clear, we can now think about **everything** that happens in the relationship to be just the comunication of that affection.

## How to comunicate affection

We can create infinite ways to express it, but the action we are seeking is the same: get our affection through to the other person, further sementing it.

In a safe and sane relationship, we can establish boundries that define _what kind_ of vectors we can use to carry our message. This boundries are by no means static, we can update them anytime, provided we do it **explicitly and consensually**.

Please note how in this process not only we define what we can do, but also what meaning we attribute to it.

In this new system, we can have an extremely emotionally intimate relationship, but where there's no touching at all, as well as a emotionally distant one, where we exclusively seek pleasure from each other, as well as a emotionally intimate, where we alternate comforting and supporting each other to kissing and comunicating affection through plesure, wich is quite different from the pleasure of the second one.

Also note that, since we could have _simultaneusly_ all three kinds of relationship, no one is excessively burdening on the partner with responsibility of our needs, since if the third doesn't feel like comforting there's always the first and if the second doesn't feel like fucking there's always the third.

