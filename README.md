# Psycho Philosophy

Rants, reflections and just my insanity slowly unraveling and encompassing everything

## Index
* [License Notice](#license-notice)
* [Structure](#structure)
* [Why](#why)
* [Start Here](#start-here)
* [Contributing](#contributing)

## License Notice
Although I included the GPLv3-or-later in this repo, more because of the habit than anything, since this isn't code but thoughts, you can basically do whatever you want with this so long that you don't publish it commercially or that you copyright it in any other way.

This must remain public (not that anyone cares, but it's a matter of respect and principle that I'm naive enough to uphold)

## Structure
I'll upload a bunch of standalone markdown files, you can read them individually, or start from one of the [starting points](#start-here) and then follow the hypertext, wandering through my head

## Why
I spend way too much time thinking, it would be a waste not to write down some of it - not necessairily the best, just some - and while I'm at it I might as well make it accessible by me on any device by putting it online.

If anyone else stubles on this (or I linked this to you, in that case you're probably so unlucky to have some sort of direct connection with me, and you know most of this shit, or you're in some random chat room where I wanna rant but don't wanna repeat the same shit I already wrote), I wish you a plesant stay.

If you wanna reach out because you're worried of my mental health or just to chat, [dm me on matrix](https://matrix.to/#/@cadmio:matrix.org)

By creating this I'm both trying to create a little escape place _and_ to try and figure out more stuff sbout me, as forcing me to put my unordered thoughts into ordered speech, particularly in a language I'm semi-fluent but not native of, having to spend time thinking on what word to use, gives me time to think about the thought itself and better understand it.

## Start Here
Gimme time, while I upload I'll try to keep this section updated.

Note that the order is pretty much stocastic and unimportant stuff may be linked here before the real "hubs" and important thoughts.

## Contributing
This is a self-exploration effort, but it doubles as philosophic-political manifesto, so if you wanna expand on the concepts mentioned or you wanna offer different views feel free to either [dm me](https://matrix.to/#/@cadmio:matrix.org) or even clone the thing and throw a pull request.

Just know I'm extremely fond of some of this, so I might reject everything. Also I might not check this for months, so idk you do you.
With that said, feel free to do what you want with this so long as you respect the [conditions](#license-notice).

You can paste quotes in your internet arguments, you can use it as a starting point of your own reflections or you can treat it as erotica and jerk off to it.

